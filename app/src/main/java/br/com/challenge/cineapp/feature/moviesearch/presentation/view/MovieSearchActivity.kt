package br.com.challenge.cineapp.feature.moviesearch.presentation.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import br.com.challenge.cineapp.R
import br.com.challenge.cineapp.core.base.BaseActivity
import br.com.challenge.cineapp.core.helper.observe
import br.com.challenge.cineapp.feature.moviedetail.presentation.view.MovieDetailActivity
import br.com.challenge.cineapp.feature.moviesearch.data.entity.MovieItem
import br.com.challenge.cineapp.feature.moviesearch.presentation.view.movielist.MovieListAdapter
import br.com.challenge.cineapp.feature.moviesearch.presentation.viewmodel.MovieSearchViewModel
import kotlinx.android.synthetic.main.act_movie_search.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.view.KeyEvent


class MovieSearchActivity : BaseActivity(), MovieListAdapter.ClickListener{

    private val viewModel by viewModel<MovieSearchViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_movie_search)

        init()
    }

    private fun init() {
        setToolbar()
        bindObservers()
        setSearchButtonClickListener()
        setSearchEditTextListener()
    }

    private fun requestMoviesByTitle(title: String) {
        viewModel.requestMoviesByTitle(title)
    }

    private fun bindObservers() {
        viewModel.getMovieItems().observe(this,
            onSuccess = {
                if(it.isNullOrEmpty().not()) {
                    showSuccessLayout()
                    populateMovies(it)
                } else {
                    showErrorLayout()
                }
            },
            onError = {
                showErrorLayout()
            },
            onLoading = {
                showLoadingLayout()
            })
    }

    override fun onClick(id: String) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.MOVIE_ID, id)
        startActivity(intent)
    }

    private fun setSearchButtonClickListener() {
        searchButton.setOnClickListener {
            requestMoviesByTitle(searchMovieEditText.text.toString())
            hideKeyboard()
        }
    }

    private fun setSearchEditTextListener() {
        searchMovieEditText.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    requestMoviesByTitle(searchMovieEditText.text.toString())
                    hideKeyboard()
                    return true
                }
                return false
            }
        })
    }

    private fun hideKeyboard() {
        val view = findViewById<View>(android.R.id.content)
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun populateMovies(movies: List<MovieItem>) {
        val movieListAdapter = MovieListAdapter(movies, this)
        moviesRecyclerView.layoutManager = GridLayoutManager(this, 3)
        moviesRecyclerView.adapter = movieListAdapter
    }

    private fun showSuccessLayout() {
        successGroup.visibility = View.VISIBLE
        includeLoadingLayout.visibility = View.GONE
        includeErrorLayout.visibility = View.GONE
    }

    private fun showErrorLayout() {
        successGroup.visibility = View.GONE
        includeLoadingLayout.visibility = View.GONE
        includeErrorLayout.visibility = View.VISIBLE
    }

    private fun showLoadingLayout() {
        successGroup.visibility = View.GONE
        includeLoadingLayout.visibility = View.VISIBLE
        includeErrorLayout.visibility = View.GONE
    }

    private fun setToolbar() {
        setToolbar(includeToolbar as Toolbar, R.string.search_movie_title)
    }
}
