package br.com.challenge.cineapp.core.di

val modules = listOf(
    repositoryModule,
    viewModelModule,
    serviceModule,
    networkModule
)