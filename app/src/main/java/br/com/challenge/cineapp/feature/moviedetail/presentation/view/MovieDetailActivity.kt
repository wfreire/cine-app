package br.com.challenge.cineapp.feature.moviedetail.presentation.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import br.com.challenge.cineapp.R
import br.com.challenge.cineapp.core.base.BaseActivity
import br.com.challenge.cineapp.core.helper.observe
import br.com.challenge.cineapp.core.ui.glide.GlideApp
import br.com.challenge.cineapp.feature.moviedetail.data.entity.Movie
import br.com.challenge.cineapp.feature.moviedetail.presentation.viewmodel.MovieDetailViewModel
import br.com.challenge.cineapp.feature.movietrailer.presentation.view.MovieTrailerActivity
import kotlinx.android.synthetic.main.act_movie_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MovieDetailActivity : BaseActivity() {

    private val viewModel by viewModel<MovieDetailViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_movie_detail)

        init(intent)
    }

    private fun init(intent: Intent) {
        setToolbar()
        bindObservers()
        retrieveMovie(intent)
    }

    private fun retrieveMovie(intent: Intent) {
        if(intent.hasExtra(MOVIE_ID)) {
            val id = intent.getStringExtra(MOVIE_ID)
            if(id != null) {
                requestMovieById(id)
            } else {
                showErrorLayout()
            }
        } else {
            showErrorLayout()
        }
    }

    private fun requestMovieById(id: String) {
        viewModel.requestMovieById(id)
    }

    private fun bindObservers() {
        viewModel.getMovieDetail().observe(this,
            onSuccess = {
                showSuccessLayout()
                populateMovie(it)
                setTrailerButtonClick()
            },
            onError = {
                showErrorLayout()
            },
            onLoading = {
                showLoadingLayout()
            })
    }

    private fun setTrailerButtonClick() {
        trailerButton.setOnClickListener {
            val intent = Intent(this, MovieTrailerActivity::class.java)
            intent.putExtra(MovieTrailerActivity.TRAILER_URL, TRAILER_URL)
            startActivity(intent)
        }
    }

    private fun populateMovie(movie: Movie) {
        titleMovieTextView.text = movie.title
        yearTextView.text = movie.year
        ratedTextView.text = movie.rated
        releasedMovieTextView.text = movie.released
        runtimeTextView.text = movie.runtime
        genreTextView.text = movie.genre
        directorTextView.text = movie.director
        writerMovieTextView.text = movie.writer
        actorTextView.text = movie.actors
        plotTextView.text = movie.plot
        languageTextView.text = movie.language
        countryTextView.text = movie.country
        awardsTextView.text = movie.awards
        metascoreTextView.text = movie.metascore
        typeTextView.text = movie.type
        dvdTextView.text = movie.dvd
        boxOfficeTextView.text = movie.boxOffice
        productionTextView.text = movie.production
        websiteTextView.text = movie.website

        GlideApp.with(this)
            .load(movie.poster)
            .placeholder(R.color.colorNero)
            .error(R.drawable.ic_camera_white)
            .into(logoImageView)
    }

    private fun showSuccessLayout() {
        successContainer.visibility = View.VISIBLE
        includeLoadingLayout.visibility = View.GONE
        includeErrorLayout.visibility = View.GONE
    }

    private fun showErrorLayout() {
        successContainer.visibility = View.GONE
        includeLoadingLayout.visibility = View.GONE
        includeErrorLayout.visibility = View.VISIBLE
    }

    private fun showLoadingLayout() {
        successContainer.visibility = View.GONE
        includeLoadingLayout.visibility = View.VISIBLE
        includeErrorLayout.visibility = View.GONE
    }

    private fun setToolbar() {
        setToolbar(includeToolbar as Toolbar, R.string.app_name, View.OnClickListener { onBackPressed() })
    }

    companion object {
        const val MOVIE_ID = "MOVIE_ID"
        private const val TRAILER_URL = "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
    }
}
