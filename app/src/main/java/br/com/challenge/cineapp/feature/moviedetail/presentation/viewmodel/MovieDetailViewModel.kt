package br.com.challenge.cineapp.feature.moviedetail.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.challenge.cineapp.core.extension.error
import br.com.challenge.cineapp.core.extension.loading
import br.com.challenge.cineapp.core.extension.success
import br.com.challenge.cineapp.core.helper.Resource
import br.com.challenge.cineapp.feature.moviedetail.data.entity.Movie
import br.com.challenge.cineapp.feature.moviedetail.data.repository.MovieDetailRepository
import kotlinx.coroutines.launch

class MovieDetailViewModel(private val movieDetailRepository: MovieDetailRepository): ViewModel() {

    private var movieDetailMutableLiveData = MutableLiveData<Resource<Movie>>()

    fun requestMovieById(id: String) {
        viewModelScope.launch {
            movieDetailMutableLiveData.loading()
            try {
                val movie = movieDetailRepository.getMovieById(id)
                movieDetailMutableLiveData.success(movie)
            } catch (e: Exception) {
                movieDetailMutableLiveData.error(e)
            }
        }
    }

    fun getMovieDetail(): LiveData<Resource<Movie>> {
        return movieDetailMutableLiveData
    }
}