package br.com.challenge.cineapp.feature.splashscreen.presentation.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import br.com.challenge.cineapp.R
import br.com.challenge.cineapp.feature.moviesearch.presentation.view.MovieSearchActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_splash_screen)

        Handler().postDelayed(this::redirectToMainActivity, 2000)
    }

    private fun redirectToMainActivity() {
        val intent = Intent(this, MovieSearchActivity::class.java)
        startActivity(intent)
        finish()
    }
}
