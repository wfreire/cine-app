package br.com.challenge.cineapp.feature.moviesearch.data.repository.remote

import br.com.challenge.cineapp.feature.moviesearch.data.entity.SearchResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieSearchEndPoint {

    @GET("/")
    suspend fun getMoviesByTitle(@Query("apikey") apiKey: String,
                                 @Query("type") type: String,
                                 @Query("s") search: String): Response<SearchResponse>

}