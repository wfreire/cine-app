package br.com.challenge.cineapp.core.di

import br.com.challenge.cineapp.BuildConfig
import br.com.challenge.cineapp.feature.moviedetail.data.repository.remote.MovieDetailEndPoint
import br.com.challenge.cineapp.feature.moviesearch.data.repository.remote.MovieSearchEndPoint
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

val networkModule = module {
    factory { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    factory { provideMovieSearchEndPoint(get()) }
    factory { provideMovieDetailEndPoint(get()) }
}

fun provideOkHttpClient(): OkHttpClient {
    val client = OkHttpClient().newBuilder()

    if (BuildConfig.DEBUG) {
        client.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
    }
    return client.build()
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl("http://omdbapi.com").client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create()).build()
}

fun provideMovieSearchEndPoint(retrofit: Retrofit): MovieSearchEndPoint = retrofit.create(MovieSearchEndPoint::class.java)

fun provideMovieDetailEndPoint(retrofit: Retrofit): MovieDetailEndPoint = retrofit.create(MovieDetailEndPoint::class.java)
