package br.com.challenge.cineapp.feature.moviesearch.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.challenge.cineapp.core.extension.error
import br.com.challenge.cineapp.core.extension.loading
import br.com.challenge.cineapp.core.extension.success
import br.com.challenge.cineapp.core.helper.Resource
import br.com.challenge.cineapp.feature.moviesearch.data.entity.MovieItem
import br.com.challenge.cineapp.feature.moviesearch.data.repository.MovieSearchRepository
import kotlinx.coroutines.launch

class MovieSearchViewModel(private val movieSearchRepository: MovieSearchRepository): ViewModel() {

    private var movieItemsMutableLiveData = MutableLiveData<Resource<List<MovieItem>>>()

    fun requestMoviesByTitle(title: String) {
        viewModelScope.launch {
            movieItemsMutableLiveData.loading()
            try {
                val movies = movieSearchRepository.getMoviesByTitle(title)
                movieItemsMutableLiveData.success(movies)
            } catch (e: Exception) {
                movieItemsMutableLiveData.error(e)
            }
        }
    }

    fun getMovieItems(): LiveData<Resource<List<MovieItem>>> {
        return movieItemsMutableLiveData
    }
}