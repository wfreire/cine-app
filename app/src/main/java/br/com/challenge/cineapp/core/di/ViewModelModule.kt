package br.com.challenge.cineapp.core.di

import br.com.challenge.cineapp.feature.moviedetail.presentation.viewmodel.MovieDetailViewModel
import br.com.challenge.cineapp.feature.moviesearch.presentation.viewmodel.MovieSearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MovieSearchViewModel(get()) }
    viewModel { MovieDetailViewModel(get()) }
}