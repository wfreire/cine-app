package br.com.challenge.cineapp.core.di

import br.com.challenge.cineapp.feature.moviedetail.data.repository.MovieDetailRepository
import br.com.challenge.cineapp.feature.moviedetail.data.repository.MovieDetailRepositoryImpl
import br.com.challenge.cineapp.feature.moviedetail.data.repository.remote.MovieDetailService
import br.com.challenge.cineapp.feature.moviesearch.data.repository.MovieSearchRepository
import br.com.challenge.cineapp.feature.moviesearch.data.repository.MovieSearchRepositoryImpl
import br.com.challenge.cineapp.feature.moviesearch.data.repository.remote.MovieSearchService
import org.koin.dsl.module

val repositoryModule = module {
    single { provideMovieSearchRepository(get()) }
    single { provideMovieDetailRepository(get()) }
}

fun provideMovieSearchRepository(movieSearchService: MovieSearchService)
        : MovieSearchRepository =
    MovieSearchRepositoryImpl(movieSearchService)

fun provideMovieDetailRepository(movieDetailService: MovieDetailService)
        : MovieDetailRepository =
    MovieDetailRepositoryImpl(movieDetailService)