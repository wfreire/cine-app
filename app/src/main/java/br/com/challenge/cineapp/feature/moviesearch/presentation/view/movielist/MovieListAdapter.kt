package br.com.challenge.cineapp.feature.moviesearch.presentation.view.movielist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.challenge.cineapp.R
import br.com.challenge.cineapp.core.ui.glide.GlideApp
import br.com.challenge.cineapp.feature.moviesearch.data.entity.MovieItem
import kotlinx.android.synthetic.main.item_movie_option.view.*

class MovieListAdapter(private val movieItems: List<MovieItem>,
                       private val listener: ClickListener): RecyclerView.Adapter<MovieListAdapter.MovieItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_movie_option, parent, false)
        return MovieItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movieItems.size
    }

    override fun onBindViewHolder(holder: MovieItemViewHolder, position: Int) {
        holder.bind(movieItems[position])
    }

    inner class MovieItemViewHolder(private var view : View): RecyclerView.ViewHolder(view) {

        fun bind(movie: MovieItem) {
            GlideApp.with(view)
                .load(movie.poster)
                .placeholder(R.color.colorNero)
                .error(R.drawable.ic_camera_white)
                .into(view.moviePosterImageView)

            view.movieTitleTextView.text = movie.title
            view.movieItemContainer.setOnClickListener {
                movie.imdbID?.let { id ->
                    listener.onClick(id)
                }
            }
        }
    }

    interface ClickListener {
        fun onClick(id: String)
    }
}

