package br.com.challenge.cineapp.core.base

import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.res.ResourcesCompat
import br.com.challenge.cineapp.R
import kotlinx.android.synthetic.main.include_toolbar.view.*


abstract class BaseActivity: AppCompatActivity() {

    protected fun setToolbar(toolbar: Toolbar,@StringRes title: Int) {
        toolbar.titleToolbarTextView.text = getString(title)
    }

    protected fun setToolbar(toolbar: Toolbar, @StringRes title: Int, clickListener: View.OnClickListener) {
        toolbar.titleToolbarTextView.text = getString(title)
        toolbar.navigationIcon = ResourcesCompat.getDrawable(resources, R.drawable.ic_left_arrow, null)
        toolbar.setNavigationOnClickListener(clickListener)
    }
}