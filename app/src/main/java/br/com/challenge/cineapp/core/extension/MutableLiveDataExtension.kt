package br.com.challenge.cineapp.core.extension

import androidx.lifecycle.MutableLiveData
import br.com.challenge.cineapp.core.helper.Resource

fun <T> MutableLiveData<Resource<T>>.success(data: T?) {
  postValue(Resource.success(data))
}

fun <T> MutableLiveData<Resource<T>>.error(e: Exception?) {
  value = Resource.error(e)
}

fun <T> MutableLiveData<Resource<T>>.loading() {
  value = Resource.loading()
}