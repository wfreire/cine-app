package br.com.challenge.cineapp.feature.movietrailer.presentation.view

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import br.com.challenge.cineapp.R
import io.clappr.player.Player
import io.clappr.player.base.Event
import io.clappr.player.base.Options

class MovieTrailerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_movie_trailer)

        init(intent)
    }

    private fun init(intent: Intent) {
        retrieveTrailer(intent)
    }

    private fun retrieveTrailer(intent: Intent) {
        if(intent.hasExtra(TRAILER_URL)) {
            val url = intent.getStringExtra(TRAILER_URL)
            if(url != null) {
                showVideoByUrl(url)
            } else {
                showError()
            }
        } else {
            showError()
        }
    }

    private fun showVideoByUrl(url: String) {
        val player = Player()
        player.off(Event.REQUEST_FULLSCREEN.value)
        player.off(Event.EXIT_FULLSCREEN.value)
        enterFullscreen(player)

        player.on(Event.ERROR.value) {
            showError()
        }

        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.playerContainer, player)
        fragmentTransaction.commit()

        player.configure(Options(source = url))
        player.play()
    }

    private fun enterFullscreen(player: Player) {
        this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE
        player.fullscreen = true
    }

    private fun showError() {
        Toast.makeText(this, R.string.trailer_error_message, Toast.LENGTH_LONG).show()
        finish()
    }

    companion object {
        const val TRAILER_URL = "TRAILER_URL"
    }
}
