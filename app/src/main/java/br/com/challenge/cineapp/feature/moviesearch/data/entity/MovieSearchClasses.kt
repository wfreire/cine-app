package br.com.challenge.cineapp.feature.moviesearch.data.entity

import com.squareup.moshi.Json

data class SearchResponse(@field:Json(name = "Search")
                          val movieItems: List<MovieItem>,
                          @field:Json(name = "totalResults")
                          val totalResults: String?,
                          @field:Json(name = "Response")
                          val response: String?)

data class MovieItem(@field:Json(name = "Title")
                     val title: String?,
                     @field:Json(name = "Year")
                     val year: String?,
                     @field:Json(name = "imdbID")
                     val imdbID: String?,
                     @field:Json(name = "Type")
                     val type: String?,
                     @field:Json(name = "Poster")
                     val poster: String?)