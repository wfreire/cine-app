package br.com.challenge.cineapp.feature.moviedetail.data.repository

import br.com.challenge.cineapp.feature.moviedetail.data.entity.Movie
import br.com.challenge.cineapp.feature.moviedetail.data.repository.remote.MovieDetailService

class MovieDetailRepositoryImpl(private val movieDetailService: MovieDetailService): MovieDetailRepository {

    override suspend fun getMovieById(id: String): Movie {
        val response = movieDetailService.getMovieDetailById(id)
        if(response.isSuccessful) {
            val movie = response.body()
            if(movie != null && movie.response.equals("True", ignoreCase = true)) {
                return movie
            } else {
                throw Exception("SearchResponse invalid")
            }
        } else {
            throw Exception("Response not successful")
        }
    }
}