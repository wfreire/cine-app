package br.com.challenge.cineapp.feature.moviedetail.data.repository

import br.com.challenge.cineapp.feature.moviedetail.data.entity.Movie

interface MovieDetailRepository {
    suspend fun getMovieById(id: String): Movie
}