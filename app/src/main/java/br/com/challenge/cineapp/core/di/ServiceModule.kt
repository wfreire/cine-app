package br.com.challenge.cineapp.core.di

import br.com.challenge.cineapp.feature.moviedetail.data.repository.remote.MovieDetailService
import br.com.challenge.cineapp.feature.moviesearch.data.repository.remote.MovieSearchService
import org.koin.dsl.module

val serviceModule = module {
    factory { MovieSearchService(get()) }
    factory { MovieDetailService(get()) }
}