package br.com.challenge.cineapp.feature.moviesearch.data.repository

import br.com.challenge.cineapp.feature.moviesearch.data.entity.MovieItem

interface MovieSearchRepository {
    suspend fun getMoviesByTitle(title: String): List<MovieItem>?
}