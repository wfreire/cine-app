package br.com.challenge.cineapp.feature.moviesearch.data.repository

import br.com.challenge.cineapp.feature.moviesearch.data.entity.MovieItem
import br.com.challenge.cineapp.feature.moviesearch.data.repository.remote.MovieSearchService

class MovieSearchRepositoryImpl(private val movieSearchService: MovieSearchService): MovieSearchRepository {

    override suspend fun getMoviesByTitle(title: String): List<MovieItem>? {
        val response = movieSearchService.getMoviesByTitle(title)
        if(response.isSuccessful) {
            val searchResponse = response.body()
            if(searchResponse != null && searchResponse.response.equals("True", ignoreCase = true)) {
                return searchResponse.movieItems
            } else {
                throw Exception("SearchResponse invalid")
            }
        } else {
            throw Exception("Response not successful")
        }
    }
}