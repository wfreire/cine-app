package br.com.challenge.cineapp.feature.moviedetail.data.repository.remote

import br.com.challenge.cineapp.core.base.BaseService.Companion.API_KEY
import br.com.challenge.cineapp.core.base.BaseService.Companion.MOVIE
import br.com.challenge.cineapp.feature.moviedetail.data.entity.Movie
import retrofit2.Response

class MovieDetailService(private val movieDetailEndPoint: MovieDetailEndPoint) {

    suspend fun getMovieDetailById(id: String): Response<Movie> {
        return movieDetailEndPoint.getMovieDetailById(API_KEY, MOVIE, FULL, id)
    }

    companion object {
        const val FULL = "full"
    }
}