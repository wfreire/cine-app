package br.com.challenge.cineapp.feature.moviesearch.data.repository.remote

import br.com.challenge.cineapp.core.base.BaseService.Companion.API_KEY
import br.com.challenge.cineapp.core.base.BaseService.Companion.MOVIE
import br.com.challenge.cineapp.feature.moviesearch.data.entity.SearchResponse
import retrofit2.Response

class MovieSearchService(private val movieSearchEndPoint: MovieSearchEndPoint) {

    suspend fun getMoviesByTitle(title: String): Response<SearchResponse> {
        return movieSearchEndPoint.getMoviesByTitle(API_KEY, MOVIE, title)
    }
}