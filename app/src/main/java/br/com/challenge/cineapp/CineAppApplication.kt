package br.com.challenge.cineapp

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import br.com.challenge.cineapp.core.di.modules
import io.clappr.player.Player
import org.koin.core.context.startKoin

class CineAppApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@CineAppApplication)
            modules(modules)
        }

        Player.initialize(this)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}