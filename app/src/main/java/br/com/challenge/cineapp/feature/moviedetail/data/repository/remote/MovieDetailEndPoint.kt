package br.com.challenge.cineapp.feature.moviedetail.data.repository.remote

import br.com.challenge.cineapp.feature.moviedetail.data.entity.Movie
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MovieDetailEndPoint {

    @GET("/")
    suspend fun getMovieDetailById(@Query("apikey") apiKey: String,
                                   @Query("type") type: String,
                                   @Query("plot") plot: String,
                                   @Query("i") id: String): Response<Movie>
}