package br.com.challenge.cineapp.feature.moviesearch.presentation.view

import android.app.Instrumentation
import android.content.ComponentName
import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent
import androidx.test.espresso.intent.matcher.IntentMatchers.hasExtra
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.platform.app.InstrumentationRegistry
import br.com.challenge.cineapp.R
import br.com.challenge.cineapp.feature.moviedetail.presentation.view.MovieDetailActivity
import br.com.challenge.cineapp.feature.moviesearch.data.entity.MovieItem
import br.com.challenge.cineapp.feature.moviesearch.data.repository.MovieSearchRepository
import br.com.challenge.cineapp.feature.moviesearch.presentation.view.movielist.MovieListAdapter
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.allOf
import org.koin.core.KoinComponent
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.mockito.ArgumentMatchers.anyString

fun onMovieSearch(block: MovieSearchSetupRobot.() -> Unit) = MovieSearchSetupRobot().apply(block)

class MovieSearchSetupRobot: KoinComponent {

    private val repository: MovieSearchRepository = mockk()

    init {
        loadKoinModules(module(override = true) {
            single { repository }
        })
    }

    fun setupPopulateMovieSearchList() {
        every { runBlocking { repository.getMoviesByTitle(any()) } } returns stubPopulateMovieSearchList()
    }

    fun setupEmptyMovieSearchList() {
        every { runBlocking { repository.getMoviesByTitle(any()) } } returns stubEmptyMovieSearchList()
    }

    infix fun launch(block: MovieSearchActionRobot.() -> Unit): MovieSearchActionRobot {
        return MovieSearchActionRobot().apply(block)
    }
}

class MovieSearchActionRobot {

    fun search() {
        onView(withId(R.id.searchMovieEditText))
            .perform(typeText(anyString()))

        onView(withId(R.id.searchButton))
            .perform(click())

        InstrumentationRegistry.getInstrumentation().waitForIdleSync()
    }

    fun clickItem(position: Int){
        onView(withId(R.id.moviesRecyclerView))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<MovieListAdapter.MovieItemViewHolder>(
                    position, click()
                )
            )
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()
    }

    infix fun check(block: MovieSearchResultRobot.() -> Unit) {
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()

        MovieSearchResultRobot().apply { block }
    }
}

class MovieSearchResultRobot {

    fun isMovieDetailScreen(movieId: String?) {
         allOf(
             hasComponent(
                 ComponentName(
                     InstrumentationRegistry.getInstrumentation().targetContext,
                     MovieDetailActivity::class.java
                 )
             ),
             hasExtra(MovieDetailActivity.MOVIE_ID, movieId))
    }

    fun hasMovieList() {
        onView(withId(R.id.moviesRecyclerView)).check(matches(isDisplayed()))
    }

    fun showErrorLayout() {
        onView(withId(R.id.includeErrorLayout)).check(matches(isDisplayed()))
    }

}

fun stubPopulateMovieSearchList(): List<MovieItem> {

    val movieSearchList = ArrayList<MovieItem>()

    movieSearchList.add(
        MovieItem(
            "Fifty Shades of Grey",
            "2015",
            "tt2322441",
            "movie",
            "https://m.media-amazon.com/images/M/MV5BMjE1MTM4NDAzOF5BMl5BanBnXkFtZTgwNTMwNjI0MzE@._V1_SX300.jpg")
    )

    movieSearchList.add(
        MovieItem(
            "The Grey",
            "2011",
            "tt1601913",
            "movie",
            "https://m.media-amazon.com/images/M/MV5BNDY4MTQwMzc1MV5BMl5BanBnXkFtZTcwNzcwNTM5Ng@@._V1_SX300.jpg")
    )

    movieSearchList.add(
        MovieItem(
            "Grey Gardens",
            "1975",
            "tt0073076",
            "movie",
            "https://m.media-amazon.com/images/M/MV5BNjQ0YzYwMzUtZjc5NS00OGQ3LWJjMmUtYmY5N2M3ZTA0NTY2XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg")
    )

    movieSearchList.add(
        MovieItem(
            "The Grey Zone",
            "2001",
            "tt0252480",
            "movie",
            "https://m.media-amazon.com/images/M/MV5BMTgzNjQzMTQ0NF5BMl5BanBnXkFtZTYwNjA0Mjc3._V1_SX300.jpg")
    )

    return movieSearchList
}

private fun stubEmptyMovieSearchList(): List<MovieItem> {
    return ArrayList()
}