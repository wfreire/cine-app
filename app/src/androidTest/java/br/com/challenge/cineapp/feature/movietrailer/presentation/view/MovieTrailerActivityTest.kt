package br.com.challenge.cineapp.feature.movietrailer.presentation.view

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.rule.ActivityTestRule
import br.com.challenge.cineapp.R
import org.junit.Rule
import org.junit.Test

class MovieTrailerActivityTest {

    @get:Rule
    var activityRule: ActivityTestRule<MovieTrailerActivity>
            = ActivityTestRule(MovieTrailerActivity::class.java, true, false)

    @Test
    fun testOpenActivityWithMovieUrlArgument() {
        val intent = Intent()
        intent.putExtra(MovieTrailerActivity.TRAILER_URL, "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8")
        activityRule.launchActivity(intent)

        Thread.sleep(3000)

        onView(withId(R.id.playerContainer))
            .check(matches(isDisplayed()))
    }
}