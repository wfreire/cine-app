package br.com.challenge.cineapp.feature

import android.app.Application
import androidx.test.platform.app.InstrumentationRegistry
import org.koin.android.ext.koin.androidContext
import org.koin.android.logger.AndroidLogger
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import br.com.challenge.cineapp.core.di.modules
import org.koin.core.context.loadKoinModules

class TestApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(InstrumentationRegistry.getInstrumentation().targetContext)
            logger(AndroidLogger())
        }

        loadKoinModules(
            modules
        )
    }

    override fun onTerminate() {
        stopKoin()
        super.onTerminate()
    }
}
