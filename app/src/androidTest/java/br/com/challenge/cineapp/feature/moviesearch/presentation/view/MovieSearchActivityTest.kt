package br.com.challenge.cineapp.feature.moviesearch.presentation.view

import android.content.ComponentName
import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import br.com.challenge.cineapp.R
import br.com.challenge.cineapp.feature.moviedetail.presentation.view.MovieDetailActivity
import br.com.challenge.cineapp.feature.moviesearch.data.entity.MovieItem
import br.com.challenge.cineapp.feature.moviesearch.data.repository.MovieSearchRepository
import br.com.challenge.cineapp.feature.moviesearch.presentation.view.movielist.MovieListAdapter
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.KoinComponent
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.mockito.ArgumentMatchers.anyString

class MovieSearchActivityTest: KoinComponent {

    @get:Rule
    var activityRule: ActivityTestRule<MovieSearchActivity>
            = ActivityTestRule(MovieSearchActivity::class.java, true, false)

    private val repository: MovieSearchRepository = mockk()

    @Before
    fun setup() {
        loadKoinModules(module(override = true) {
            single { repository }
        })
    }

    @Test
    fun testHasMovieList() {
        onMovieSearch {
            setupPopulateMovieSearchList()

            activityRule.launchActivity(Intent())
        } launch {
            search()
        } check {
            hasMovieList()
        }
    }

    @Test
    fun testEmptyMovieBySearch() {
        onMovieSearch {
            setupEmptyMovieSearchList()

            activityRule.launchActivity(Intent())
        } launch {
            search()
        } check {
            showErrorLayout()
        }
    }

    @Test
    fun testGoToMovieDetail() {
        onMovieSearch {
            setupPopulateMovieSearchList()

            activityRule.launchActivity(Intent())
        } launch {
            search()
            clickItem(2)
        } check {
            val movies = stubPopulateMovieSearchList()
            isMovieDetailScreen(movies[2].imdbID)
        }
    }

    @Test
    fun testHasMovieListOld() {
        every { runBlocking { repository.getMoviesByTitle(any()) } } returns stubPopulateMovieSearchList()

        activityRule.launchActivity(Intent())

        onView(withId(R.id.searchMovieEditText))
            .perform(typeText(anyString()))

        onView(withId(R.id.searchButton))
            .perform(click())

        InstrumentationRegistry.getInstrumentation().waitForIdleSync()

        onView(withId(R.id.moviesRecyclerView))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testEmptyMovieBySearchOld() {
        every { runBlocking { repository.getMoviesByTitle(any()) } } returns stubEmptyMovieSearchList()

        activityRule.launchActivity(Intent())

        onView(withId(R.id.searchMovieEditText))
            .perform(typeText(anyString()))

        onView(withId(R.id.searchButton))
            .perform(click())

        InstrumentationRegistry.getInstrumentation().waitForIdleSync()

        onView(withId(R.id.includeErrorLayout))
            .check(matches(isDisplayed()))
    }

    @Test
    fun testGoToMovieDetailOld() {
        every { runBlocking { repository.getMoviesByTitle(any()) } } returns stubPopulateMovieSearchList()

        activityRule.launchActivity(Intent())

        onView(withId(R.id.searchMovieEditText))
            .perform(typeText(anyString()))

        onView(withId(R.id.searchButton))
            .perform(click())

        InstrumentationRegistry.getInstrumentation().waitForIdleSync()

        onView(withId(R.id.moviesRecyclerView))
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<MovieListAdapter.MovieItemViewHolder>(2, click())
            )

        InstrumentationRegistry.getInstrumentation().waitForIdleSync()

        val movies = stubPopulateMovieSearchList()
        allOf(
            IntentMatchers.hasComponent(
                ComponentName(
                    InstrumentationRegistry.getInstrumentation().targetContext,
                    MovieDetailActivity::class.java
                )
            ),
            IntentMatchers.hasExtra(MovieDetailActivity.MOVIE_ID, movies[2].imdbID)
        )
    }

    private fun stubPopulateMovieSearchList(): List<MovieItem> {

        val movieSearchList = ArrayList<MovieItem>()

        movieSearchList.add(
            MovieItem(
                "Fifty Shades of Grey",
                "2015",
                "tt2322441",
                "movie",
                "https://m.media-amazon.com/images/M/MV5BMjE1MTM4NDAzOF5BMl5BanBnXkFtZTgwNTMwNjI0MzE@._V1_SX300.jpg")
        )

        movieSearchList.add(
            MovieItem(
                "The Grey",
                "2011",
                "tt1601913",
                "movie",
                "https://m.media-amazon.com/images/M/MV5BNDY4MTQwMzc1MV5BMl5BanBnXkFtZTcwNzcwNTM5Ng@@._V1_SX300.jpg")
        )

        movieSearchList.add(
            MovieItem(
                "Grey Gardens",
                "1975",
                "tt0073076",
                "movie",
                "https://m.media-amazon.com/images/M/MV5BNjQ0YzYwMzUtZjc5NS00OGQ3LWJjMmUtYmY5N2M3ZTA0NTY2XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg")
        )

        movieSearchList.add(
            MovieItem(
                "The Grey Zone",
                "2001",
                "tt0252480",
                "movie",
                "https://m.media-amazon.com/images/M/MV5BMTgzNjQzMTQ0NF5BMl5BanBnXkFtZTYwNjA0Mjc3._V1_SX300.jpg")
        )

        return movieSearchList
    }

    private fun stubEmptyMovieSearchList(): List<MovieItem> {
        return ArrayList()
    }
}