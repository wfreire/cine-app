package br.com.challenge.cineapp.feature.moviedetail.presentation.view

import android.content.Intent
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import br.com.challenge.cineapp.R
import br.com.challenge.cineapp.feature.moviedetail.data.entity.Movie
import br.com.challenge.cineapp.feature.moviedetail.data.entity.Rating
import br.com.challenge.cineapp.feature.moviedetail.data.repository.MovieDetailRepository
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.koin.core.KoinComponent
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.mockito.ArgumentMatchers

fun onMovieDetail(block: MovieDetailSetupRobot.() -> Unit) = MovieDetailSetupRobot().apply(block)

class MovieDetailSetupRobot : KoinComponent {

    private val repository: MovieDetailRepository = mockk()

    init {
        loadKoinModules(module(override = true) {
            single { repository }
        })
    }

    fun setupMovieDetail() {
        every { runBlocking { repository.getMovieById(any()) } } returns stubMovie()
    }

    fun setMovieIdExtra(movieId: String?): Intent {
        val intent = Intent()
        intent.putExtra(MovieDetailActivity.MOVIE_ID, movieId)
        return intent
    }

    infix fun launch(block: MovieActionRobot.() -> Unit): MovieActionRobot {
        return MovieActionRobot().apply(block)
    }
}

class MovieActionRobot {

    infix fun check(block: MovieResultRobot.() -> Unit) {
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()

        MovieResultRobot().apply(block)
    }
}

class MovieResultRobot {

    fun isShowSuccessLayout() {
        onView(withId(R.id.successContainer)).check(matches(isDisplayed()))
    }

    fun isShowErrorLayout() {
        onView(withId(R.id.includeErrorLayout)).check(matches(isDisplayed()))
    }

    fun hasLogoImage() {
        onView(withId(R.id.logoImageView)).check(matches(isDisplayed()))
    }

    fun hasTitle() {
        onView(withId(R.id.titleMovieTextView)).check(matches(isDisplayed()))
    }

    fun hasNoTitle() {
        onView(withId(R.id.titleMovieTextView)).check(matches(withText("")))
    }
}

private fun stubMovie(): Movie {

    val listRating = ArrayList<Rating>()
    listRating.add(Rating( "Internet Movie Database", "6.8/10"))
    listRating.add(Rating( "Rotten Tomatoes", "79%"))
    listRating.add(Rating( "Metacritic", "64/100"))

    return Movie("The Grey",
        "2011",
        "R",
        "27 Jan 2012",
        "117 min",
        "Action, Adventure, Drama, Thriller",
        "Joe Carnahan",
        "Joe Carnahan (screenplay), Ian Mackenzie Jeffers (screenplay), Ian Mackenzie Jeffers (short story Ghost Walker)",
        "Liam Neeson, Frank Grillo, Dermot Mulroney, Dallas Roberts",
        "In Alaska, a team of oil workers board a flight home; however, they cross a storm and the airplane crashes. Only seven workers survive in the wilderness and John Ottway, who is a huntsman that kills wolves to protect the workers, assumes leadership of the group. Shortly after they learn that they are surrounded by a pack of wolves and Ottway advises that they should seek protection in the woods. But while they walk through the heavy snow, they are chased and attacked by the carnivorous mammals.",
        "English, Spanish",
        "USA",
        "2 wins & 8 nominations.",
        "https://m.media-amazon.com/images/M/MV5BNDY4MTQwMzc1MV5BMl5BanBnXkFtZTcwNzcwNTM5Ng@@._V1_SX300.jpg",
        listRating,
        "64",
        "6.8",
        "232,920",
        "tt1601913",
        "movie",
        "15 May 2012",
        "$51,533,608",
        "Open Road Films",
        "",
        "N/A",
        "True",
        "")
}